#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include"List.h"

#define MAX_LEN 300

int main(int argc, char * argv[]){
    int line_count, inputLength, tempCount;
    FILE *inputFile, *outputFile, *testFile;
    char line[MAX_LEN];
    char * inputString, tempString, subString;
    List indexList, newindexList;

    if( argc != 3 ){
        printf("Not enough arguments");
        exit(EXIT_FAILURE);
    }

    testFile = fopen(argv[1], "r");
    if( testFile == NULL) {
        printf("NULL input file");
        exit(EXIT_FAILURE);
    }

    outputFile = fopen(argv[2], "w");
    if( outputFile == NULL) {
        printf("NULL output file");
        exit(EXIT_FAILURE);
    }

    indexList = newList();
    line_count = 0;
    inputLength = 0;
    while( fgets(line, MAX_LEN, testFile) != NULL)  {
        inputLength++;
    }
    fclose(testFile);
    inputFile = fopen(argv[1], "r");
    char **stringArray = malloc(MAX_LEN);
    for (int i = 0; i < inputLength; ++i){
        stringArray[i] = malloc(MAX_LEN);
    }

    while( fgets(line, MAX_LEN, inputFile) != NULL)  {
        line_count++;
        fprintf(outputFile,"%s", line);
        strcpy(stringArray[line_count-1], line);
    }

    for(int i=0;i<inputLength;i++){
        for(int j=0;j<MAX_LEN;j++){
            printf("%c", stringArray[i][j]);
        }
    }
    
    for(int i=0; i<inputLength; i++){
        for(int j=0; j<inputLength; j++){
            if(strcmp(stringArray[i], stringArray[j]) > 0){
                tempCount++;
            }
        }
        append(indexList, tempCount);      
        tempCount = 0;
    }
    int i = length(indexList);
    while (i > 0 && strcmp(stringArray[i], stringArray[indexList->data[i+1]]) < 0) {
        l->data[i] = l->data[i-1];
        i--;
    }
    for(int i=0; i<length(indexList); i++){
        (indexList = 0;
    }

    printList(stdout, indexList);
    fclose(inputFile);
    fclose(outputFile);

    return(0);
}