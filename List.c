//-----------------------------------------------------------------------------
// List.c
// Implementation file for List ADT
//-----------------------------------------------------------------------------
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include "List.h"

// structs --------------------------------------------------------------------

// private Node type
typedef struct NodeObj* Node;

// private NodeObj type
typedef struct NodeObj{
   ListElement data;
   Node prev;
   Node next;
} NodeObj;

// private ListObj type
typedef struct ListObj{
   Node front;
   Node back;
   int length;
   Node cursor;
   int index;
} ListObj;

// Constructors-Destructors ---------------------------------------------------

// newNode()
// Returns reference to new Node object. Initializes next and data fields.
Node newNode(ListElement data){
   Node N = malloc(sizeof(ListObj));
   assert( N!=NULL ); 
   N->data = data;
   N->next = NULL;
   N->prev = NULL;
   return(N);
}

// freeNode()
// Frees heap memory pointed to by *pN, sets *pN to NULL.
void freeNode(Node* pN){
   if( pN!=NULL && *pN!=NULL ){
      free(*pN); 
      *pN = NULL;
   }
}

// newList()
// Creates and returns a new empty List.
List newList(void){
   List L;
   L = malloc(sizeof(ListObj));
   assert( L!=NULL );
   L->front = L->back = NULL;
   L->length = 0;
   L->cursor = NULL;
   L->index = -1; 
   return(L);
}

// freeList()
// Frees all heap memory associated with List *pQ, and sets *pQ to NULL.
void freeList(List* pQ){
   if(pQ!=NULL && *pQ!=NULL) { 
      while( length(*pQ) > 0) { 
         deleteFront(*pQ); 
      }
      free(*pQ);
      *pQ = NULL;
   }
}

// Access functions -----------------------------------------------------------

// length()
// Returns the length of Q.
int length(List L){
   if( L==NULL ){
      printf("List Error: calling getLength() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   return(L->length);
}

// Returns index of cursor element if defined, -1 otherwise.
int index(List L){
    if( L==NULL ){
      printf("List Error: calling getFront() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   return L->index;
}

// Returns front element of L. Pre: length()>0
int front(List L){
   if( L==NULL ){
      printf("List Error: calling getFront() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling front() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   return(L->front->data);
}

// Returns back element of L. Pre: length()>0
int back(List L){
   if( L==NULL ){
      printf("List Error: calling back() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if(length(L) <= 0){
       printf("List Error: calling back() on empty List reference\n");
       exit(EXIT_FAILURE);
   }
   return(L->back->data);
}

// Returns cursor element of L. Pre: length()>0, index()>=0
int get(List L){
   if( L==NULL ){
      printf("List Error: calling get() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if(length(L) <= 0){
       printf("List Error: calling get() on empty List reference\n");
       exit(EXIT_FAILURE);
   }
   if(L->index < 0){
      printf("List Error: calling get() on List with no defined cursor\n");
       exit(EXIT_FAILURE);
   }
   return L->cursor->data;
}


// Returns true if Lists A and B are in same
// state, and returns false otherwise.
bool equals(List A, List B){
   if( A==NULL || B==NULL ){
      printf("List Error: calling equals() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   bool eq;
   Node N, M;
   eq = ( A->length == B->length );
   N = A->front;
   M = B->front;
   while( eq && N!=NULL){
      eq = ( N->data==M->data );
      N = N->next;
      M = M->next;
   }
   return eq;
}

// Manipulation procedures ----------------------------------------------------

// Resets L to its original empty state
void clear(List L){
   if( L==NULL ){
      printf("List Error: calling clear() on NULL List reference\n");
      exit(EXIT_FAILURE);
   } 
   while( length(L) > 0) { 
      deleteFront(L); 
   }
   L = NULL;
}

// Overwrites the cursor element’s data with x.
// Pre: length()>0, index()>=0
void set(List L, int x){
   Node N = newNode(x);
   if( L==NULL ){
      printf("List Error: calling set() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( L->index < 0 ){
      printf("List Error: calling set() on an undefined cursor List\n");
      exit(EXIT_FAILURE);
   }
   L->cursor = N;
   
}

// If L is non-empty, sets cursor under the front element, otherwise does nothing.
void moveFront(List L){
   if( L==NULL ){
      printf("List Error: calling moveFront() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling moveFront() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   L->cursor = L->front;
   L->index = 0;
}

// If L is non-empty, sets cursor under the back element, otherwise does nothing.
void moveBack(List L){
   if( L==NULL ){
      printf("List Error: calling moveBack() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling moveBack() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   L->cursor = L->back;
   L->index = length(L) -1 ;
}

// If cursor is defined and not at front, move cursor one
// step toward the front of L; if cursor is defined and at
// front, cursor becomes undefined; if cursor is undefined do nothing
void movePrev(List L){
   if( L==NULL ){
      printf("List Error: calling movePrev() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling movePrev() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   if(L->cursor->prev == NULL){
      //L->cursor->data = -1;
      L->index = -1;
   }
   if(L->cursor != NULL){
      L->cursor = L->cursor->prev;
      L->index --;
   }
}

// If cursor is defined and not at back, move cursor one
// step toward the back of L; if cursor is defined and at
// back, cursor becomes undefined; if cursor is undefined
// do nothing
void moveNext(List L){
   if( L==NULL ){
      printf("List Error: calling moveNext() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling moveNext() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   if(L->cursor->next == NULL){
      //L->cursor->data = -1; 
      //L->index = -1;
      L->cursor = NULL;
   }
   else if(L->cursor->data >= 0){
      L->cursor = L->cursor->next;
      L->index ++;
   }
}

// Insert new element into L. If L is non-empty, insertion takes place after back element
void append(List L, int x){
   Node N = newNode(x);
   
   if( L==NULL ){
      printf("List Error: calling apppend() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   
   if( length(L) <= 0) { 
      L->front = L->back = N; 
   }else{
      N->prev = L->back;
      L->back->next = N; 
      L->back = N;  
   }
   L->length++;
}

// Insert new element into L. If L is non-empty, insertion takes place before front element
void prepend(List L, int x){
   Node N = newNode(x);
  
   if( L==NULL ){
      printf("List Error: calling prepend() on NULL List reference\n"); 
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0) { 
      L->front = L->back = N; 
   }else{ 
      L->front->prev = N;
      N->next = L->front;
      L->front = N;
       
   }
   L->length++;
}

// Insert new element before cursor.
// Pre: length()>0, index()>=0
void insertBefore(List L, int x){
   Node N = NULL;
   Node S = newNode(x);
   if( L==NULL ){
      printf("List Error: calling insertBefore() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling insertBefore() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   if( L->index < 0){
      printf("List Error: calling insertBefore() on a list with an undefined cursor\n");
      exit(EXIT_FAILURE);
   }/*
   else if(L->cursor == NULL){
      printf("cursor null but index not -1");
      exit(EXIT_FAILURE);
   }*/
   if((index(L) != 0) &  (L->index != L->length)){
      N = L->front;
      while(N->data != get(L)){
         N = N->next;
      }
      N->prev->next = S;
      S->next = N;
      L->length++;
   }
   if(index(L) == 0){
      prepend(L, S->data);
   }
   else if(L->index == L->length){
      append(L, x);
   }
}

// Insert new element after cursor.
// Pre: length()>0, index()>=0
void insertAfter(List L, int x){
   Node N = NULL;
   Node S = newNode(x);
   if( L==NULL ){
      printf("List Error: calling insertAfter() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling insertAfter() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   if( L->index < 0){
      printf("List Error: calling insertAfter() on a list with an undefined cursor\n");
      exit(EXIT_FAILURE);
   }
   else if(L->cursor == NULL){
      printf("cursor null but index not -1");
      exit(EXIT_FAILURE);
   }
   N = L->front;
   while(N->data != get(L)){
      N = N->next;
   }
   S->next = N->next;
   N->next = S;
   S->prev = N;
   L->length++;
}

// Delete the front element. Pre: length()>0
void deleteFront(List L){
   Node N = NULL;
   if( L==NULL ){
      printf("List Error: calling deleteFront() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling deleteFront() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   N = L->front;
   if( length(L)>1 ) { 
      L->front = L->front->next; 
   }else{ 
      L->front = L->back = NULL; 
   }
   L->length--;
   freeNode(&N);
}

// Delete the back element. Pre: length()>0
void deleteBack(List L){
   Node N = NULL;
   if( L==NULL ){
      printf("List Error: calling deleteBack() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( length(L) <= 0 ){
      printf("List Error: calling deleteBack() on an empty List\n");
      exit(EXIT_FAILURE);
   }
   N = L->back;
   if( length(L)>1 ) { 
      L->back = L->back->prev;
      L->back->next = NULL;
   }else{ 
      L->front = L->back = NULL; 
   }
   L->length--;
   freeNode(&N);
}

// Delete cursor element, making cursor undefined.
// Pre: length()>0, index()>=0
void delete(List L){
   Node N = NULL;
   if( L==NULL ){
      printf("List Error: calling delete() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   if( L->index < 0 ){
      printf("List Error: calling delete() on an undefined cursor List\n");
      exit(EXIT_FAILURE);
   }
   N = L->front;
   while(N->data != get(L)){
      N = N->next;
   }
   if(L->cursor == 0){
      L->front = L->front->next;
   }
   if(N->next == NULL){
      deleteBack(L);
   }
   if(N->next != NULL){
      N->next->prev = N->prev;
   }
   if(N->prev == NULL){
      deleteFront(L);
   }
   if(N->prev != NULL){
      N->prev->next = N->next;
   }
   L->cursor = NULL;
   if((N->next != NULL) & (N->prev != NULL)){
      freeNode(&N);
   }
}

// Other Functions ------------------------------------------------------------

//void printList()
// Prints to the file pointed to by out, a string representation of L consisting
// of a space separated sequence of integers, with front on left.
void printList(FILE* out ,List L){
   Node N = NULL;
   if( L==NULL ){
      printf("List Error: calling printList() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   for(N = L->front; N != NULL; N = N->next){
      fprintf(out, FORMAT" ", N->data);
   }
}

// Returns a new List representing the same integer
// sequence as L. The cursor in the new list is undefined,
// regardless of the state of the cursor in L. The state
// of L is unchanged.
List copyList(List L){
   List C = newList();
   //printf(FORMAT" ", C);
   Node N = NULL;
   if( L==NULL){
      printf("List Error: calling copylist() on NULL List reference\n");
      exit(EXIT_FAILURE);
   }
   N = L->front;
   while(N->data != L->back->data){
      append(C, N->data);
      N = N->next;
   }
   if(N->data == L->back->data){
      append(C, N->data);
   }
   return C;
}